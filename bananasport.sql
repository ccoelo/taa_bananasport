-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Dim 02 Mars 2014 à 16:13
-- Version du serveur: 5.6.12-log
-- Version de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `bananasport`
--
CREATE DATABASE IF NOT EXISTS `bananasport` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `bananasport`;

-- --------------------------------------------------------

--
-- Structure de la table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateEnvoi` date DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `destinataire_id` int(11) DEFAULT NULL,
  `expediteur_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK9C2397E7CAFC7AF3` (`destinataire_id`),
  KEY `FK9C2397E750A2238D` (`expediteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `parcours`
--

CREATE TABLE IF NOT EXISTS `parcours` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `parcours_pointgps`
--

CREATE TABLE IF NOT EXISTS `parcours_pointgps` (
  `parcours_id` int(11) NOT NULL,
  `pointsGps_id` int(11) NOT NULL,
  KEY `FK3A140F10D2DE9962` (`pointsGps_id`),
  KEY `FK3A140F10416C0A0F` (`parcours_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `pointgps`
--

CREATE TABLE IF NOT EXISTS `pointgps` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `x` double NOT NULL,
  `y` double NOT NULL,
  `z` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `seance`
--

CREATE TABLE IF NOT EXISTS `seance` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `calorie` double NOT NULL,
  `distance` double NOT NULL,
  `duree` double NOT NULL,
  `vitesse` double NOT NULL,
  `sport_id` int(11) DEFAULT NULL,
  `utilisateur_id` int(11) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK935F42C14B210865` (`sport_id`),
  KEY `FK935F42C1FFD00DC5` (`utilisateur_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `seance`
--

INSERT INTO `seance` (`id`, `calorie`, `distance`, `duree`, `vitesse`, `sport_id`, `utilisateur_id`, `commentaire`, `date`) VALUES
(1, 10, 10, 10, 10, 1, 1, 'Super séance!!! J''étais très en forme.', '2014-01-08'),
(2, 20, 20, 20, 20, 3, 1, 'Super course, j''ai été très rapide!', '2014-01-09'),
(3, 1, 1, 1, 1, 2, 2, 'J''ai été trop fort au judo aujourd''hui!', '2014-01-13'),
(4, 1, 1, 1, 1, NULL, NULL, NULL, NULL),
(5, 100, 10, 10, 50, 4, 1, 'Petite balade en vélo!', '2014-03-03');

-- --------------------------------------------------------

--
-- Structure de la table `sport`
--

CREATE TABLE IF NOT EXISTS `sport` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Contenu de la table `sport`
--

INSERT INTO `sport` (`id`, `nom`) VALUES
(1, 'natation'),
(2, 'judo'),
(3, 'Course'),
(4, 'vélo');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE IF NOT EXISTS `utilisateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dateInscription` date DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `nom` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `poids` double NOT NULL,
  `prenom` varchar(255) DEFAULT NULL,
  `pseudo` varchar(255) DEFAULT NULL,
  `sexe` varchar(255) DEFAULT NULL,
  `taille` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `dateInscription`, `dateNaissance`, `mail`, `nom`, `password`, `poids`, `prenom`, `pseudo`, `sexe`, `taille`) VALUES
(1, '2013-11-21', '1990-07-22', 'c.coelo@mail.com', 'coelo', 'password', 100, 'christophe', 'cricri', 'M', 2),
(2, NULL, '1990-07-22', 'toto@mail.com', 'toto', 'toto', 2, 'toto', 'toto', 'M', 2),
(3, NULL, '1990-07-22', 'tata@mail.com', 'tata', 'tatatata', 10, 'tata', 'tata', 'M', 10),
(4, NULL, '1990-07-22', 'titi@mail.com', 'titi', 'titititi', 10, 'titi', 'titi', 'F', 10),
(5, NULL, '1990-07-22', 'tutu@mail.com', 'tutu', 'tututututu', 10, 'tutu', 'tutu', 'F', 10);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `message`
--
ALTER TABLE `message`
  ADD CONSTRAINT `FK9C2397E750A2238D` FOREIGN KEY (`expediteur_id`) REFERENCES `utilisateur` (`id`),
  ADD CONSTRAINT `FK9C2397E7CAFC7AF3` FOREIGN KEY (`destinataire_id`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `parcours_pointgps`
--
ALTER TABLE `parcours_pointgps`
  ADD CONSTRAINT `FK3A140F10416C0A0F` FOREIGN KEY (`parcours_id`) REFERENCES `parcours` (`id`),
  ADD CONSTRAINT `FK3A140F10D2DE9962` FOREIGN KEY (`pointsGps_id`) REFERENCES `pointgps` (`id`);

--
-- Contraintes pour la table `seance`
--
ALTER TABLE `seance`
  ADD CONSTRAINT `FK935F42C14B210865` FOREIGN KEY (`sport_id`) REFERENCES `sport` (`id`),
  ADD CONSTRAINT `FK935F42C1FFD00DC5` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
