'use strict';

describe('Service: Authmanager', function () {

  // load the service's module
  beforeEach(module('yoApp'));

  // instantiate service
  var Authmanager;
  beforeEach(inject(function (_Authmanager_) {
    Authmanager = _Authmanager_;
  }));

  it('should do something', function () {
    expect(!!Authmanager).toBe(true);
  });

});
