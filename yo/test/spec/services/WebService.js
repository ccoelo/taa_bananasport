'use strict';

describe('Service: Webservice', function () {

  // load the service's module
  beforeEach(module('yoApp'));

  // instantiate service
  var Webservice;
  beforeEach(inject(function (_Webservice_) {
    Webservice = _Webservice_;
  }));

  it('should do something', function () {
    expect(!!Webservice).toBe(true);
  });

});
