'use strict';

describe('Controller: MessagerieCtrl', function () {

  // load the controller's module
  beforeEach(module('yoApp'));

  var MessagerieCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    MessagerieCtrl = $controller('MessagerieCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
