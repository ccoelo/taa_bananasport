'use strict';

  angular.module('yoApp').controller('EditprofilCtrl',['$scope','$location', 'Webservice', 'Authmanager', function($scope, $location, Webservice, Authmanager) {
	if(Authmanager.isAuthenticated()){
		Webservice.getUserByID($scope);
	}else{
		$location.path('/');
	};

	$scope.editProfil = function(user){
		Webservice.updateUser(user,this.editCallback);
	};

	$scope.editCallback = function(data){
		console.log("CallBackok edit");
	};

}]);
