'use strict';
angular.module('yoApp').controller('SeanceCtrl', ['$scope', '$location','Webservice', 'Authmanager', function ($scope, $location, Webservice, Authmanager) {
	var next = 1;
	if(Authmanager.isAuthenticated()){
		Webservice.getSports($scope,this.sportCallback);
		Webservice.getSeances($scope,Authmanager.getUserId(),this.seancesCallback);
		
		$scope.sportCallback = function(data){
			$scope.sports=data;
		};

		$scope.seancesCallback = function(data){
			$scope.seanceslist=data;
		};
		
		$scope.newSeance = function(seance){
			//var parcours = this.getGPSPoints();
			//seance.utilisateur = Authmanager.getUser();
			Webservice.createSeance(this.newSeanceCallback,seance);
		};
		
		$scope.newSeanceCallback = function(bool){
			if(!bool){
				$scope.infoNewMessage = "Error";
			}else{
				$scope.infoNewMessage = "Seance successfully Created";
			}
		};
		$scope.addFormField = function(){
			   var addto = "#fieldY" + next;
				next = next + 1;
				var newIn = '<br /><br /><input class="span3" id="fieldX' + next + '" name="fieldX' + next + '" type="text" placeholder="Point X" ng-model="pointX'+next+'">' 
				+'	<input class="span3" ng-model="pointY'+next+'" id="fieldY' + next + '" name="fieldY' + next + '" type="text"  placeholder="Point Y">'+
				'  <button id="b'+next+'" onClick="$(this).prev().remove();$(this).prev(2).remove();$(this).remove();" class="btn btn-info" type="button">-</button>';
				var newInput = $(newIn);
				$(addto).after(newInput);
				if(next>1)
					$("button#b"+next).after(newInput);
				$("#fieldY" + next).attr('data-source',$(addto).attr('data-source'));
				$("#count").val(next); 
		};
		
		$scope.getGPSPoints = function(){
				var parcours = {gpsPoints: []};
				var cpt = 1;
				while(cpt<=next){
					parcours.gpsPoints.push({"latitude" : angular.element( document.querySelector( '#fieldX'+cpt ) )[0].value , "longitude" : angular.element( document.querySelector( '#fieldY'+cpt ) )[0].value});
					cpt++;
				}
				return parcours;
		};
		
	}else{
		$location.path('/');
	}
  }]);
