'use strict';

angular.module('yoApp').controller('DetailCtrl',['$scope','$location', 'Webservice', 'Authmanager', function($scope, $location, Webservice, Authmanager) {
	if(Authmanager.isAuthenticated()){
		console.log("user :"+Authmanager.getUserId())
		Webservice.getUserByID($scope,Authmanager.getUserId());
	}else{
		$location.path('/');
	};

	$scope.deleteUser = function($data){
		console.log("user delete");
		Webservice.deleteUser(data);
	}

}]);