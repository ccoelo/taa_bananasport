'use strict';

angular.module('yoApp')
  .controller('MessagerieCtrl', function ($scope, Authmanager, Webservice) {
	if(Authmanager.isAuthenticated()){
		$scope.typeView = "view";
		/*$scope.messages = [
		  {dateEnvoi:'John', expediteur:25, message:'boy'},
		  {dateEnvoi:'Jessie', expediteur:30, message:'girl'},
		  {dateEnvoi:'Johanna', expediteur:28, message:'girl'},
		  {dateEnvoi:'Joy', expediteur:15, message:'girl'}
		];*/
		$scope.messages = function(){
				return Webservice.getMessages();
		}
		
		$scope.getMessagesCallback = function($msg){
				$scope.messages = $msg
		}
		
		$scope.newMessage = function(){
			$scope.typeView = "new";
		};
		
		$scope.deleteMessage = function(){
			console.log("ok delete message");
		};
		
		$scope.sendNewMessage = function(newMessage){
			Webservice.sendMessage(newMessage,this.sendNewMessageCallback);
		};
		
		$scope.sendNewMessageCallback = function(bool){
			if(bool){
				$scope.typeView = "view";
				$scope.infoNewMessage = "Message successfully sent";
			}else{
				$scope.infoNewMessage = "Error";
			}
		}
	}
	
  });
