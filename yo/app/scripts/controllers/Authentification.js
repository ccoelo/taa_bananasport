'use strict';
var authenticate = false;
var userID = null;
angular.module('yoApp')
  .controller('AuthentificationCtrl',['$scope', '$location', 'Webservice', 'Authmanager',function ($scope,$location, Webservice, Authmanager) {
		$scope.master = {};
		
		$scope.login = function($authParams){
			Authmanager.setAuthenticate(true,1);
			Webservice.login($authParams,this.loginCallback);
			$location.path('detail');
		};
		
		$scope.loginCallback = function($id){
			console.log("loginCallback : "+$id)
			if($id!=false){
				Authmanager.setAuthenticate(true,$id);
				$location.path('detail');
			}else{
				Authmanager.setAuthenticate(false);
				$scope.errorAuth = "Votre email ou mot de passe est incorrect";
			}
		};
		
		$scope.isAuthUnchanged = function(authParams) {
			return angular.equals(authParams, $scope.master);
		};
		
		$scope.isAuthenticate = function() {
			return Authmanager.isAuthenticated();
		};
		
		$scope.logout = function(){
			Authmanager.setAuthenticate(false);
			$location.path('/');
		};
		
		$scope.logoutCallback = function(bool){
			if(bool){
				Authmanager.setAuthenticate(false);
				$location.path('/');
			}
		};
  }]);
