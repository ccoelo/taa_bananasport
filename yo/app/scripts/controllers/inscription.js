'use strict';

var INTEGER_REGEXP = /^\-?\d*$/;
angular.module('yoApp').directive('integer', function() {
	  return {
		require: 'ngModel',
		link: function(scope, elm, attrs, ctrl) {
		  ctrl.$parsers.unshift(function(viewValue) {
			if (INTEGER_REGEXP.test(viewValue)) {
			  // it is valid
			  ctrl.$setValidity('integer', true);
			  return viewValue;
			} else {
			  // it is invalid, return undefined (no model update)
			  ctrl.$setValidity('integer', false);
			  return undefined;
			}
		  });
		}
	  };
	});
	
	angular.module('yoApp').directive("passwordVerify", function() {
	   return {
		  require: "ngModel",
		  scope: {
			passwordVerify: '='
		  },
		  link: function(scope, element, attrs, ctrl) {
			scope.$watch(function() {
				var combined;

				if (scope.passwordVerify || ctrl.$viewValue) {
				   combined = scope.passwordVerify + '_' + ctrl.$viewValue; 
				}                    
				return combined;
			}, function(value) {
				if (value) {
					ctrl.$parsers.unshift(function(viewValue) {
						var origin = scope.passwordVerify;
						if (origin !== viewValue) {
							ctrl.$setValidity("passwordVerify", false);
							return undefined;
						} else {
							ctrl.$setValidity("passwordVerify", true);
							return viewValue;
						}
					});
				}
			});
		 }
	   };
	});
	
angular.module('yoApp')
  .controller('InscriptionCtrl', function ($scope, Webservice) {
    $scope.master = {};
	$scope.datePattern=/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/i;
	
	$scope.update = function(user) {
		$scope.master = angular.copy(user);
	};
	
	$scope.signin = function(user){
		Webservice.newUser(user,this.signinCallback);
	};
	
	$scope.signinCallback = function(data){
		console.log("CallBackok toto");
	};
	
	$scope.isUnchanged = function(user) {
		return angular.equals(user, $scope.master);
	};
	
	$scope.authenticate = function(username,password){
		Webservice.authenticate(username,password);
	}

  });

