'use strict';

angular.module('yoApp', [ 'ngCookies', 'ngResource', 'ngSanitize', 'ngRoute' ])
		.config(function($routeProvider) {
			$routeProvider.when('/', {
				templateUrl : 'views/main.html',
				controller : 'MainCtrl'
			}).when('/detail', {
				templateUrl : 'views/detail.html',
				controller : 'DetailCtrl'
			}).when('/inscription', {
				templateUrl : 'views/inscription.html',
				controller : 'InscriptionCtrl'
			}).when('/seance', {
				templateUrl : 'views/seance.html',
				controller : 'SeanceCtrl'
			}).when('/messagerie', {
				templateUrl : 'views/messagerie.html',
				controller : 'MessagerieCtrl'
			}).when('/timeline', {
				templateUrl: 'views/timeline.html',
				controller: 'TimelineCtrl'
			}).when('/editProfil', {
			  templateUrl: 'views/editProfil.html',
			  controller: 'EditprofilCtrl'
			}).otherwise({
				redirectTo : '/'
			});
		});
