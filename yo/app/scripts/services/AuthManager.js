'use strict';
angular.module('yoApp')
  .service('Authmanager', function Authmanager($cookieStore) {
	  if(!$cookieStore.get('IsAuthenticated')){
		$cookieStore.IsAuthenticated = false;
		//$cookieStore.userID = appel rest 
	  }
	return {
		setAuthenticate : function(bool,id){
			$cookieStore.put('IsAuthenticated', bool);
			$cookieStore.put('userId', id);
			//$cookieStore.IsAuthenticated = bool;
		},
		
		isAuthenticated : function(){
			var isAuthenticated = $cookieStore.get('IsAuthenticated');//$cookieStore.IsAuthenticated;
			//isAuthenticated = true;
			return isAuthenticated;
		},
		
		getUserId : function(){
			return $cookieStore.get('userId');
		}
	}
  });
