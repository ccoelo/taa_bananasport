/**
 * 
 */
package com.istic.taa.bananasport.util;

import java.io.Serializable;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

/**
 * @author Christophe
 *
 */
public class HeaderBuilder {
	
	public HeaderBuilder(){
	}
	
	public ResponseBuilder getResponse(Serializable q){
		ResponseBuilder builder = Response.ok(q);

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		return builder;
	}

}
