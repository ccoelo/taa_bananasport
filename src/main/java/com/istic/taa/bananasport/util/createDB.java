package com.istic.taa.bananasport.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class createDB {

	public static void main(String[] args) {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("bananasportUnit");
		EntityManager em = emf.createEntityManager(); 
		EntityTransaction tx = null; 
		try { 
			tx = em.getTransaction(); 
			tx.begin(); 
			tx.commit();
		} catch (RuntimeException e) { 
				if (tx != null && tx.isActive()) 
					tx.rollback(); 
				throw e; 
		} finally { em.close(); } 
			emf.close(); 
		}
	}

