/**
 * 
 */
package com.istic.taa.bananasport.util;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @author Christophe
 *
 */
public class PersistenceManager {

	private static EntityManagerFactory emf;
	
	public static EntityManagerFactory getEntityManagerFactory(){
		
		if(emf == null){
			emf= Persistence.createEntityManagerFactory("bananasportUnit");
		}
		return emf;
	}
	
	public static void closeEntityManagerFactory(){
		if(emf !=null && emf.isOpen()){
			emf.close();
		}
	}
}
