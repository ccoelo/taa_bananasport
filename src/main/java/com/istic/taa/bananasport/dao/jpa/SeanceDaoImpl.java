/**
 * 
 */
package com.istic.taa.bananasport.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.istic.taa.bananasport.dao.SeanceDao;
import com.istic.taa.bananasport.entity.Seance;

/**
 * @author Christophe
 * 
 */
public class SeanceDaoImpl implements SeanceDao {

	private EntityManagerFactory emf;

	public SeanceDaoImpl(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public Seance add(Seance object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public List<Seance> findAll() {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u from Seance u");
			return query.getResultList();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public Seance update(Seance object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.refresh(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public Seance delete(Seance object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

}
