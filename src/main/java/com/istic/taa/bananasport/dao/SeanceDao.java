/**
 * 
 */
package com.istic.taa.bananasport.dao;

import com.istic.taa.bananasport.entity.Seance;

/**
 * @author Christophe
 *
 */
public interface SeanceDao extends Dao<Seance>{

}
