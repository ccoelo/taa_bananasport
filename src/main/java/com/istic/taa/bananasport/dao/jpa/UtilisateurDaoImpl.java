/**
 * 
 */
package com.istic.taa.bananasport.dao.jpa;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.istic.taa.bananasport.dao.UtilisateurDao;
import com.istic.taa.bananasport.entity.Seance;
import com.istic.taa.bananasport.entity.Utilisateur;

/**
 * @author Christophe
 * 
 */
public class UtilisateurDaoImpl implements UtilisateurDao {

	private EntityManagerFactory emf;

	public UtilisateurDaoImpl(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public Utilisateur add(Utilisateur object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public List<Utilisateur> findAll() {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u from Utilisateur u");
			return query.getResultList();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public Utilisateur update(Utilisateur object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.refresh(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public Utilisateur delete(Utilisateur object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public Utilisateur findById(int id) {
		EntityManager em = emf.createEntityManager();
		try {

			em.getTransaction().begin();
			Utilisateur u = em.find(Utilisateur.class, id);
			System.out.println("toto");
			return u;
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	public List<Seance> getSeances() {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u.seances from Utilisateur u");
			return (List<Seance>) query.getResultList();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	public Utilisateur login(String email) {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u from Utilisateur u where u.mail=:email");
			query.setParameter("email", email);
			Utilisateur u = (Utilisateur) query.getSingleResult();
			return u;
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

}
