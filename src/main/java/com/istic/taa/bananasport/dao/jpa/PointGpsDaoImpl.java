/**
 * 
 */
package com.istic.taa.bananasport.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.istic.taa.bananasport.dao.PointGpsDao;
import com.istic.taa.bananasport.entity.PointGps;

/**
 * @author Christophe
 * 
 */
public class PointGpsDaoImpl implements PointGpsDao {

	private EntityManagerFactory emf;

	public PointGpsDaoImpl(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public PointGps add(PointGps object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public List<PointGps> findAll() {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u from PointGps u");
			return query.getResultList();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public PointGps update(PointGps object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.refresh(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public PointGps delete(PointGps object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

}
