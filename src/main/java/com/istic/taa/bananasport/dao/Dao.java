/**
 * 
 */
package com.istic.taa.bananasport.dao;

import java.util.List;

import com.istic.taa.bananasport.entity.Utilisateur;

/**
 * @author Christophe
 *
 */
public interface Dao<T> {

	public T add(T object);
	
	public List<T> findAll();
	
	public T update(T object);
	
	public T delete(T object);
}
