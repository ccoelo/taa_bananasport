/**
 * 
 */
package com.istic.taa.bananasport.dao;

import com.istic.taa.bananasport.entity.Parcours;

/**
 * @author Christophe
 *
 */
public interface ParcoursDao extends Dao<Parcours>{

}
