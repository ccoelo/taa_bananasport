/**
 * 
 */
package com.istic.taa.bananasport.dao;

import com.istic.taa.bananasport.dao.jpa.MessageDaoImpl;
import com.istic.taa.bananasport.dao.jpa.ParcoursDaoImpl;
import com.istic.taa.bananasport.dao.jpa.PointGpsDaoImpl;
import com.istic.taa.bananasport.dao.jpa.SeanceDaoImpl;
import com.istic.taa.bananasport.dao.jpa.SportDaoImpl;
import com.istic.taa.bananasport.dao.jpa.UtilisateurDaoImpl;
import com.istic.taa.bananasport.util.PersistenceManager;

/**
 * @author Christophe
 *
 */
public class DaoFactory {

	private static DaoFactory instance;
	
	private UtilisateurDaoImpl utilisateurDao;
	private MessageDaoImpl messageDao;
	private SportDaoImpl sportDao;
	private ParcoursDaoImpl parcoursDao;
	private SeanceDaoImpl seanceDao;
	private PointGpsDaoImpl pointgpsDao;
	
	public static DaoFactory getIstance(){
		if(instance == null){
			instance = new DaoFactory();
		}
		return instance;
	}
	
	public UtilisateurDaoImpl getUtilisateurDao(){
		if(utilisateurDao == null){
			utilisateurDao = new UtilisateurDaoImpl(PersistenceManager.getEntityManagerFactory());
		}
		return utilisateurDao;
	}
	
	public MessageDaoImpl getMessageDao(){
		if(messageDao == null){
			messageDao = new MessageDaoImpl(PersistenceManager.getEntityManagerFactory());
		}
		return messageDao;
	}
	
	public SportDaoImpl getSportDao(){
		if(sportDao == null){
			sportDao = new SportDaoImpl(PersistenceManager.getEntityManagerFactory());
		}
		return sportDao;
	}
	
	public ParcoursDaoImpl getParcoursDao(){
		if(parcoursDao == null){
			parcoursDao = new ParcoursDaoImpl(PersistenceManager.getEntityManagerFactory());
		}
		return parcoursDao;
	}
	
	public SeanceDaoImpl getSeanceDao(){
		if(seanceDao == null){
			seanceDao = new SeanceDaoImpl(PersistenceManager.getEntityManagerFactory());
		}
		return seanceDao;
	}
	
	public PointGpsDaoImpl getPointGpsDao(){
		if(pointgpsDao == null){
			pointgpsDao = new PointGpsDaoImpl(PersistenceManager.getEntityManagerFactory());
		}
		return pointgpsDao;
	}
}
