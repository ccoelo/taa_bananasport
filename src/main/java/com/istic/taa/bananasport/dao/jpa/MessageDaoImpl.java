/**
 * 
 */
package com.istic.taa.bananasport.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.istic.taa.bananasport.dao.MessageDao;
import com.istic.taa.bananasport.entity.Message;

/**
 * @author Christophe
 * 
 */
public class MessageDaoImpl implements MessageDao {

	private EntityManagerFactory emf;

	public MessageDaoImpl(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public Message add(Message object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public List<Message> findAll() {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u from Message u");
			return query.getResultList();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public Message update(Message object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.refresh(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public Message delete(Message object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

}
