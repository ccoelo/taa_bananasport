/**
 * 
 */
package com.istic.taa.bananasport.dao.jpa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;

import com.istic.taa.bananasport.dao.ParcoursDao;
import com.istic.taa.bananasport.entity.Parcours;

/**
 * @author Christophe
 * 
 */
public class ParcoursDaoImpl implements ParcoursDao {

	private EntityManagerFactory emf;

	public ParcoursDaoImpl(EntityManagerFactory emf) {
		super();
		this.emf = emf;
	}

	@Override
	public Parcours add(Parcours object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.persist(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public List<Parcours> findAll() {
		EntityManager em = emf.createEntityManager();
		try {
			Query query = em.createQuery("select u from Parcours u");
			return query.getResultList();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
	}

	@Override
	public Parcours update(Parcours object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.refresh(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

	@Override
	public Parcours delete(Parcours object) {
		EntityManager em = emf.createEntityManager();
		try {
			em.getTransaction().begin();
			em.remove(object);
			em.getTransaction().commit();
		} finally {
			if (em.getTransaction().isActive()) {
				em.getTransaction().rollback();
			}
			em.close();
		}
		return object;
	}

}
