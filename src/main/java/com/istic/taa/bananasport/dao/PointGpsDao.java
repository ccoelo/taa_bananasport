/**
 * 
 */

package com.istic.taa.bananasport.dao;
import com.istic.taa.bananasport.entity.PointGps;

/**
 * @author Christophe
 *
 */
public interface PointGpsDao extends Dao<PointGps>{

}
