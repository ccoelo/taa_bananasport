/**
 * 
 */
package com.istic.taa.bananasport.dao;

import com.istic.taa.bananasport.entity.Sport;

/**
 * @author Christophe
 *
 */
public interface SportDao extends Dao<Sport>{
	
}
