/**
 * 
 */
package com.istic.taa.bananasport.dao;

import com.istic.taa.bananasport.entity.Utilisateur;

/**
 * @author Christophe
 *
 */
public interface UtilisateurDao extends Dao<Utilisateur>{
	
	public Utilisateur findById(int id);
	
	public Utilisateur add(Utilisateur u);

}
