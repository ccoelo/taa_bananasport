/**
 * 
 */
package com.istic.taa.bananasport.dao;

import com.istic.taa.bananasport.entity.Message;

/**
 * @author Christophe
 *
 */
public interface MessageDao extends Dao<Message>{

}
