/**
 * 
 */
package com.istic.taa.bananasport.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.jpa.ParcoursDaoImpl;
import com.istic.taa.bananasport.entity.Parcours;

/**
 * @author Christophe
 *
 */
@Path("/parcours")
public class ParcoursRest {

	private ParcoursDaoImpl parcoursDao;
	
	public ParcoursRest(){
		this.parcoursDao = DaoFactory.getIstance().getParcoursDao();
	}
	
	@GET
	@Path("/test")
	public String test(){
		return "Banana Service : parcours";
	}
	
	@GET
	@Produces("APPLICATION/JSON")
	public List<Parcours> getAll(){
		return parcoursDao.findAll();
	}
	
	@POST
	@Consumes("APPLICATION/JSON")
	public void add(Parcours parcours){
		parcoursDao.add(parcours);
	}
	
	@PUT
	@Consumes("APPLICATION/JSON")
	public void update(Parcours parcours){
		parcoursDao.update(parcours);
	}
	
	@DELETE
	@Consumes("APPLICATION/JSON")
	public void delete(Parcours parcours){
		parcoursDao.delete(parcours);
	}
	
}

