/**
 * 
 */
package com.istic.taa.bananasport.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.jpa.UtilisateurDaoImpl;
import com.istic.taa.bananasport.entity.Utilisateur;
import com.istic.taa.bananasport.util.HeaderBuilder;

/**
 * @author Christophe
 * 
 */

@Path("/utilisateur")
public class UtilisateurRest {

	private UtilisateurDaoImpl utilisateurDao;
	private HeaderBuilder builder;

	public UtilisateurRest() {
		this.utilisateurDao = DaoFactory.getIstance().getUtilisateurDao();
		this.builder = new HeaderBuilder();
	}

	@GET
	@Path("/test")
	public String test() {
		return "Banana Service : utilisateur";
	}

	@GET
	@Produces("APPLICATION/JSON")
	public List<Utilisateur> getAll() {
		return utilisateurDao.findAll();
	}

	@GET
	@Path("/{id}")
	public Response get(@PathParam("id") int id) {
		return builder.getResponse(utilisateurDao.findById(id)).build();
	}
	
	@GET
	@Path("/seances")
	public Response getSeances() {
		ResponseBuilder builder = Response.ok(utilisateurDao.getSeances());

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		return builder.build();
	}

	@POST
	@Consumes("APPLICATION/JSON")
	@Path("/add")
	public Response add(Utilisateur utilisateur) {
		ResponseBuilder builder = Response.ok(utilisateurDao.add(utilisateur));

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		return builder.build();
	}

	@PUT
	@Consumes("APPLICATION/JSON")
	@Path("/update")
	public Response update(Utilisateur utilisateur) {
		ResponseBuilder builder = Response.ok(utilisateurDao.update(utilisateur));

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		return builder.build();
	}

	@DELETE
	@Consumes("APPLICATION/JSON")
	public Response delete(Utilisateur utilisateur) {
		ResponseBuilder builder = Response.ok(utilisateurDao.delete(utilisateur));

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		return builder.build();
	}
	
	@GET
	@Path("/login/{email}")
	public Response get(@PathParam("email") String email) {
		return builder.getResponse(utilisateurDao.login(email)).build();
	}

	@OPTIONS
	@Path("/add")
	public Response getOptionsAdd() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"Content-Type, Accept, X-Requested-With").build();
	}
	
	@OPTIONS
	@Path("/update")
	public Response getOptionsUpdate() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"Content-Type, Accept, X-Requested-With").build();
	}
	
	@OPTIONS
	@Path("/delete")
	public Response getOptionsDelete() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"Content-Type, Accept, X-Requested-With").build();
	}
}
