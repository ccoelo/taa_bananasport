/**
 * 
 */
package com.istic.taa.bananasport.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.jpa.SportDaoImpl;
import com.istic.taa.bananasport.entity.Sport;

/**
 * @author Christophe
 * 
 */
@Path("/sport")
public class SportRest {

	private SportDaoImpl sportDao;

	public SportRest() {
		this.sportDao = DaoFactory.getIstance().getSportDao();
	}

	@GET
	@Produces(MediaType.TEXT_PLAIN)
	@Path("/test")
	public String test() {
		return "Banana Service : sport";
	}

	@GET
	@Produces("APPLICATION/JSON")
	public Response getAll() {
		
		ResponseBuilder builder = Response.ok(sportDao.findAll());

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		
		return builder.build();

	}

	@POST
	@Consumes("APPLICATION/JSON")
	public void add(Sport sport) {
		sportDao.add(sport);
	}

	@PUT
	@Consumes("APPLICATION/JSON")
	public void update(Sport sport) {
		sportDao.update(sport);
	}

	@DELETE
	@Consumes("APPLICATION/JSON")
	public void delete(Sport sport) {
		sportDao.delete(sport);
	}
	
	@OPTIONS
	@Path("/add")
	public Response getOptionsCreate() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"Content-Type, Accept, X-Requested-With").build();
	}

}
