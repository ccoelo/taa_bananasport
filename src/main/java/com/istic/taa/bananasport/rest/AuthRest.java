/**
 * 
 */
package com.istic.taa.bananasport.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.jpa.UtilisateurDaoImpl;
import com.istic.taa.bananasport.entity.Utilisateur;
import com.istic.taa.bananasport.util.HeaderBuilder;


@Path("/authentification")
public class AuthRest {

	private UtilisateurDaoImpl utilisateurDao;
	private HeaderBuilder builder;
	
	public AuthRest(){
		this.utilisateurDao = DaoFactory.getIstance().getUtilisateurDao();
		this.builder = new HeaderBuilder();
	}
	
	@GET
	@Path("/test")
	@Produces("text/plain")
	public String test(){
		return "Banana Service auth : message";
	}
	
	@GET
	@Produces("text/plain")
	public Response isAuthenticated(){
		ResponseBuilder builder = Response.ok("true");

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		return builder.build();
	}
	
	@POST
	@Consumes("APPLICATION/JSON")
	public boolean login(String authValues){
		/*Utilisateur u= this.utilisateurDao.findById(authValues.get(0));
		if(((Utilisateur) u).get("password").equals(authValues.get(1))){
			//JETON A retourner
			return true;
		}else{
			return true;
		}*/
		System.out.println("okkkk");
		return true;
	}
	/*
	@PUT
	@Consumes("APPLICATION/JSON")
	public void update(Message message){
		messageDao.update(message);
	}*/
	
	@DELETE
	@Consumes("APPLICATION/JSON")
	public boolean delete(String idUser){
		return true;
	}
	
}

