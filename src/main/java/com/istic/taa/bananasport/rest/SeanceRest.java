/**
 * 
 */
package com.istic.taa.bananasport.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.SeanceDao;
import com.istic.taa.bananasport.dao.jpa.SeanceDaoImpl;
import com.istic.taa.bananasport.entity.Seance;
import com.istic.taa.bananasport.util.HeaderBuilder;

/**
 * @author Christophe
 *
 */
@Path("/seance")
public class SeanceRest {

	private SeanceDaoImpl seanceDao;
	private HeaderBuilder builder;
	
	public SeanceRest(){
		this.seanceDao = DaoFactory.getIstance().getSeanceDao();
		this.builder = new HeaderBuilder();
	}
	
	@GET
	@Path("/test")
	public String test(){
		return "Banana Service : seance";
	}
	
	@GET
	@Produces("APPLICATION/JSON")
	public Response getAll(){
		ResponseBuilder builder = Response.ok(seanceDao.findAll());

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		
		return builder.build();
	}
	
	@POST
	@Consumes("APPLICATION/JSON")
	@Path("/add")
	public Response add(Seance seance){
		ResponseBuilder builder = Response.ok(seanceDao.add(seance));

		builder.header("Access-Control-Allow-Origin", "*");
		builder.header("Access-Control-Allow-Methods",
				"POST, GET, PUT, UPDATE, OPTIONS");
		builder.header("Access-Control-Allow-Headers",
				"Content-Type, Accept, X-Requested-With");
		
		return builder.build();
	}
	
	@PUT
	@Consumes("APPLICATION/JSON")
	public void update(Seance seance){
		seanceDao.update(seance);
	}
	
	@DELETE
	@Consumes("APPLICATION/JSON")
	public void delete(Seance seance){
		seanceDao.delete(seance);
	}
	
	@OPTIONS
	@Path("/add")
	public Response getOptionsAdd() {
		return Response
				.ok()
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods",
						"POST, GET, PUT, UPDATE, OPTIONS")
				.header("Access-Control-Allow-Headers",
						"Content-Type, Accept, X-Requested-With").build();
	}
	
}

