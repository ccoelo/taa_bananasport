/**
 * 
 */
package com.istic.taa.bananasport.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.jpa.PointGpsDaoImpl;
import com.istic.taa.bananasport.entity.PointGps;

/**
 * @author Christophe
 *
 */
@Path("/pointgps")
public class PointGpsRest {

	private PointGpsDaoImpl pointgpsDao;
	
	public PointGpsRest(){
		this.pointgpsDao = DaoFactory.getIstance().getPointGpsDao();
	}
	
	@GET
	@Path("/test")
	public String test(){
		return "Banana Service : pointgps";
	}
	
	@GET
	@Produces("APPLICATION/JSON")
	public List<PointGps> getAll(){
		return pointgpsDao.findAll();
	}
	
	@POST
	@Consumes("APPLICATION/JSON")
	public void add(PointGps pointgps){
		pointgpsDao.add(pointgps);
	}
	
	@PUT
	@Consumes("APPLICATION/JSON")
	public void update(PointGps pointgps){
		pointgpsDao.update(pointgps);
	}
	
	@DELETE
	@Consumes("APPLICATION/JSON")
	public void delete(PointGps pointgps){
		pointgpsDao.delete(pointgps);
	}
	
}

