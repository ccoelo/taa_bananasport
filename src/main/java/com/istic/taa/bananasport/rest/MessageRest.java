/**
 * 
 */
package com.istic.taa.bananasport.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.istic.taa.bananasport.dao.DaoFactory;
import com.istic.taa.bananasport.dao.jpa.MessageDaoImpl;
import com.istic.taa.bananasport.entity.Message;

/**
 * @author Christophe
 *
 */
@Path("/message")
public class MessageRest {

	private MessageDaoImpl messageDao;
	
	public MessageRest(){
		this.messageDao = DaoFactory.getIstance().getMessageDao();
	}
	
	@GET
	@Path("/test")
	public String test(){
		return "Banana Service : message";
	}
	
	@GET
	@Produces("APPLICATION/JSON")
	public List<Message> getAll(){
		return messageDao.findAll();
	}
	
	@POST
	@Consumes("APPLICATION/JSON")
	public void add(Message message){
		messageDao.add(message);
	}
	
	@PUT
	@Consumes("APPLICATION/JSON")
	public void update(Message message){
		messageDao.update(message);
	}
	
	@DELETE
	@Consumes("APPLICATION/JSON")
	public void delete(Message message){
		messageDao.delete(message);
	}
	
}

