package com.istic.taa.bananasport.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

/**
 * The persistent class for the pointgps database table.
 * 
 */
@Entity
@Table(name = "pointgps")
@NamedQuery(name = "Pointgp.findAll", query = "SELECT p FROM PointGps p")
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
public class PointGps implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	private double x;

	private double y;

	private double z;

	// bi-directional many-to-many association to Parcour
	@ManyToMany(mappedBy = "pointgps")
	private List<Parcours> parcours;

	public PointGps() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getX() {
		return this.x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return this.y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getZ() {
		return this.z;
	}

	public void setZ(double z) {
		this.z = z;
	}

	public List<Parcours> getParcours() {
		return this.parcours;
	}

	public void setParcours(List<Parcours> parcours) {
		this.parcours = parcours;
	}

}