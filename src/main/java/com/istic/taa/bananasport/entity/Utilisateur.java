package com.istic.taa.bananasport.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.istic.taa.bananasport.util.Sexe;


/**
 * The persistent class for the utilisateur database table.
 * 
 */
@Entity
@NamedQuery(name="Utilisateur.findAll", query="SELECT u FROM Utilisateur u")
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	@Temporal(TemporalType.DATE)
	private Date dateInscription;

	@Temporal(TemporalType.DATE)
	private Date dateNaissance;

	private String mail;

	private String nom;

	private String password;

	private double poids;

	private String prenom;

	private String pseudo;

	@Enumerated(EnumType.STRING)
	private Sexe sexe;

	private double taille;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="expediteur")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Message> messagesEnvoyes;

	//bi-directional many-to-one association to Message
	@OneToMany(mappedBy="destinataire")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Message> messagesRecu;

	//bi-directional many-to-one association to Seance
	@OneToMany(mappedBy="utilisateur")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Seance> seances;

	public Utilisateur() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateInscription() {
		return this.dateInscription;
	}

	public void setDateInscription(Date dateInscription) {
		this.dateInscription = dateInscription;
	}

	public Date getDateNaissance() {
		return this.dateNaissance;
	}

	public void setDateNaissance(Date dateNaissance) {
		this.dateNaissance = dateNaissance;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public double getPoids() {
		return this.poids;
	}

	public void setPoids(double poids) {
		this.poids = poids;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getPseudo() {
		return this.pseudo;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public Sexe getSexe() {
		return this.sexe;
	}

	public void setSexe(Sexe sexe) {
		this.sexe = sexe;
	}

	public double getTaille() {
		return this.taille;
	}

	public void setTaille(double taille) {
		this.taille = taille;
	}

	public List<Message> getMessagesEnvoyes() {
		return this.messagesEnvoyes;
	}

	public void setMessagesEnvoyes(List<Message> messagesEnvoyes) {
		this.messagesEnvoyes = messagesEnvoyes;
	}

	public Message addMessagesEnvoyes(Message messagesEnvoyes) {
		getMessagesEnvoyes().add(messagesEnvoyes);
		messagesEnvoyes.setExpediteur(this);

		return messagesEnvoyes;
	}

	public Message removeMessagesEnvoyes(Message messagesEnvoyes) {
		getMessagesEnvoyes().remove(messagesEnvoyes);
		messagesEnvoyes.setExpediteur(this);

		return messagesEnvoyes;
	}

	public List<Message> getMessagesRecu() {
		return this.messagesRecu;
	}

	public void setMessagesRecu(List<Message> messagesRecu) {
		this.messagesRecu = messagesRecu;
	}

	public Message addMessagesRecu(Message messagesRecu) {
		getMessagesRecu().add(messagesRecu);
		messagesRecu.setDestinataire(this);

		return messagesRecu;
	}

	public Message removeMessagesRecu(Message messagesRecu) {
		getMessagesRecu().remove(messagesRecu);
		messagesRecu.setDestinataire(this);

		return messagesRecu;
	}

	public List<Seance> getSeances() {
		return this.seances;
	}

	public void setSeances(List<Seance> seances) {
		this.seances = seances;
	}

	public Seance addSeance(Seance seance) {
		getSeances().add(seance);
		seance.setUtilisateur(this);

		return seance;
	}

	public Seance removeSeance(Seance seance) {
		getSeances().remove(seance);
		seance.setUtilisateur(null);

		return seance;
	}

}