package com.istic.taa.bananasport.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;


/**
 * The persistent class for the parcours database table.
 * 
 */
@Entity
@Table(name="parcours")
@NamedQuery(name="Parcour.findAll", query="SELECT p FROM Parcours p")
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
public class Parcours implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	//bi-directional many-to-many association to Pointgp
	@ManyToMany
	@JoinTable(
		name="parcours_pointgps"
		, joinColumns={
			@JoinColumn(name="parcours_id")
			}
		, inverseJoinColumns={
			@JoinColumn(name="pointsGps_id")
			}
		)
	private List<PointGps> pointgps;

	public Parcours() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<PointGps> getPointgps() {
		return this.pointgps;
	}

	public void setPointgps(List<PointGps> pointgps) {
		this.pointgps = pointgps;
	}

}