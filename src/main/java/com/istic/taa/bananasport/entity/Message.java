package com.istic.taa.bananasport.entity;

import java.io.Serializable;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.util.Date;


/**
 * The persistent class for the message database table.
 * 
 */
@Entity
@NamedQuery(name="Message.findAll", query="SELECT m FROM Message m")
@XmlRootElement
@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class)
public class Message implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	private int id;

	@Temporal(TemporalType.DATE)
	private Date dateEnvoi;

	private String message;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="expediteur_id")
	private Utilisateur expediteur;

	//bi-directional many-to-one association to Utilisateur
	@ManyToOne
	@JoinColumn(name="destinataire_id")
	private Utilisateur destinataire;

	public Message() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDateEnvoi() {
		return this.dateEnvoi;
	}

	public void setDateEnvoi(Date dateEnvoi) {
		this.dateEnvoi = dateEnvoi;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Utilisateur getExpediteur() {
		return this.expediteur;
	}

	public void setExpediteur(Utilisateur utilisateur1) {
		this.expediteur = utilisateur1;
	}

	public Utilisateur getDestinataire() {
		return this.destinataire;
	}

	public void setDestinataire(Utilisateur utilisateur2) {
		this.destinataire = utilisateur2;
	}

}